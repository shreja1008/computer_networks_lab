#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <map>
#include <queue>
#include <vector>
#include <cmath>
#include <cstring>
#include <netinet/in.h>
#include <netdb.h>/*For struct hostent*/
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>/*For read and write*/
#include <sys/types.h>
#include <sys/stat.h>

#define ERROR(x) perror(x),exit(1)
#define PORTNUM 4567

using namespace std;

/*Huffman tree node*/
struct treenode{
	treenode *left;
	treenode *right;
	/*The value from 0~255*/
	int data;
	/*The number of times of the character.*/														
	int times;
	/*To record the minimun character in the treenode.*/												
	int min;														
};

/*Wrap the array in the struct so that map can use it.*/
struct array{														
	char a[9];
	/*To record the size of the array.*/														
	int size;
	/*To record the appearence times*/
	int times;													
};

/*The priority queue comparision.*/
class mycompare{													
	public:
	bool operator()(const treenode *t1, const treenode *t2)
	{
		/*Smaller times, be put to the front of the queue*/
		if(t1->times > t2->times)									
			return true;
		/*Compare the ascii code of the character, put the samller one to the front.*/
		else if(t1->times==t2->times && t1->min > t2->min)			
			return true;
		else 
			return false;	
	}
		
};

/*================Global variables=================*/

extern map<int,int> count;					//To count the times of the characters.
extern map<int,array> encode_table;			//To record the encoding table. And can use to decoding.
extern map<array,int> decode_table;
extern unsigned char *un_code;
extern char *bit_code;
extern unsigned char *com_code;
extern int exceed;							//To record the number of excessive binary codes which are used to align the binary codes.
extern priority_queue<treenode*, vector<treenode*>, mycompare> p_queue;

/*=================================================*/
void create_p_queue();
void build_huffman_tree();
void huffman_encoding(treenode *node,char str[],int i);
int bit_with_encoding(int filelen);
int compress_data(int j);
int com_to_bit(int com_data_size);
int umcompress_data(int j);
int huffman_encoding_fixed_length(int number_of_types);