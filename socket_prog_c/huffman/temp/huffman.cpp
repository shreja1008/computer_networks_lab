#include "huffman.h"

using namespace std;

/*Create a priority queue.*/
void create_p_queue()												
{
	map<int,int>::iterator it;
		
	for(it=count.begin();it!=count.end();it++)					
	{
		treenode *newnode = new treenode;
		newnode->data = it->first;
		/*record the minimum data.*/
		newnode->min = it->first;
		newnode->times = it->second;
		newnode->left = NULL;
		newnode->right = NULL;
		p_queue.push(newnode);
	}
}

/*Build huffman tree.*/
void build_huffman_tree()											
{
	/*
	Pop two of the treenode from the priority queue.
	Generate a new node and connect the left and right to two treenodes respectively.
	At last put the new treenode to the priority queue.
	If the element in the priority queue is just only one, then the construction of tree is finished.
	*/
	while(p_queue.size()>1)
	{
		/*left tree node data always less than than the right tree node*/
		
		treenode *rootnode = new treenode;							
		treenode *leftnode = p_queue.top();
		p_queue.pop();
		treenode *rightnode = p_queue.top();
		p_queue.pop();
		rootnode->data = leftnode->data + rightnode->data;
		rootnode->times = leftnode->times + rightnode->times;
		/*Compare and find the minmum ascii code in the data.*/														
		rootnode->min = (leftnode->min<rightnode->min)?leftnode->min:rightnode->min;
		
		rootnode->left = leftnode;
		rootnode->right = rightnode;
		p_queue.push(rootnode);
	}
}

/*Fixed length encoding*/
int huffman_encoding_fixed_length(int number_of_types)
{
	int temp = number_of_types;
	int en_length = 1;
	map<int,int>::iterator it;
	/*Calulate encoding length*/
	while(temp>=2)
	{
		temp>>=1;
		en_length++;
	}

	/*Encode the character according to the time they appeared*/
	int pow_of_2 = pow(2,en_length)-1;
	for(it=count.begin();it!=count.end();it++)
	{


		int bitcode = pow_of_2;
		for(int i=0;i<en_length;i++)
		{
			if(bitcode%2==0)
				encode_table[it->first].a[i] = '0';
			else
				encode_table[it->first].a[i] = '1';
			bitcode>>=1;
		}

		encode_table[it->first].size = en_length;
		//cout<<it->second<<endl;
		encode_table[it->first].times = it->second;
		pow_of_2--;
	}
	return en_length;
}

/*Variabled lenght encoding*/
void huffman_encoding(treenode *node,char str[],int i)
{	
	/*If is on the leaf node, record the binay code.*/
	if(node->left ==NULL && node->right== NULL)						
	{
		for(int j=0;j<i;j++)
		{
			(encode_table[node->data]).a[j] = str[j];
			(encode_table[node->data]).size = j+1;
		}
		//printf("%c : %s\n",node->data,encode_table[node->data].a);
		encode_table[node->data].times = node->times;
	}
	else
	{
		/*Go left, encode with 0.*/
		if(node->left!=NULL)										
		{
			str[i] = '0';
			huffman_encoding(node->left,str,i+1);
		}
		/*Go right, encode with 1.*/
		if(node->right!=NULL)										
		{
			str[i] = '1';
			huffman_encoding(node->right,str,i+1);
		}
		
	}
}

/*Transform the umcompressed code into binary code*/
int bit_with_encoding(int filelen)
{
	int j = 0;
	for(int i=0;i<filelen;i++)
	{
		for(int k=0;k<(encode_table[un_code[i]]).size;k++)
		{
			bit_code[j] = (encode_table[un_code[i]]).a[k];
			j++;
		}
	}
	/*Fulfill the bit_code for the base of 8.*/
	while(j%8!=0)													
	{
		bit_code[j++] = '0';
		exceed++;
	}
	return j;
}

/*Compress data. Transform 8-bit into a character.*/
int compress_data(int j)											
{
	int now = 0;
	for(int i=0;i<j;)							
	{
		int bit8 = 7;
		int com_num = 0;
		/*Every 8 bits convert to a character.*/
		while(bit8>=0 && i<j)										
		{
			if(bit_code[i]=='1')
				com_num += pow(2,bit8);
			bit8--;
			i++;
		}
		
		com_code[now] = (unsigned char)com_num;
		now++;
	}
	return now;
}

/*Convert compression data to binary code.*/
int com_to_bit(int com_data_size)									
{
	
	int j=0;
	for(int i=0;i<com_data_size;i++)
	{
		int num = (int)com_code[i];
		int bit8 = pow(2,7);
		/*Convert a character to a byte(8 bits).*/							
		while(bit8!=0)												 
		{	
			/*If can be divided by bit8, add 1, else add 0.*/
			if(num/bit8>0)											
			{
				bit_code[j] = '1';
				num%=bit8;
			} 
			else
				bit_code[j] = '0';
			j++;
			bit8/=2;
		}
	}
	return j;

}

/*Umcompress data.*/
int umcompress_data(int j)											
{
	string tmp="";
	int now = 0;
	/*Have to minus the extra binary code.*/
	for(int i=0;i<j-exceed;)										
	{
		//Iterator of encode_table.
		map<int,array>::iterator it;			
		for(it=encode_table.begin();it!=encode_table.end();it++)
		{	
			
			int k = 0;												
			int z = i;
			/*To use encode_table second value(code) from 0.*/
			/*To record the bit_code position which has not been read.*/								
			while(k<it->second.size)								
			{
				/*If the binary code is different, go to next index.*/
				if(bit_code[z]!=it->second.a[k])					
					break;
				z++;
				k++;	
			}
			/*If the whole binary codes are the same, record it.*/
			if(k>=it->second.size)									
			{
				un_code[now] = (unsigned char)it->first;
				i+=k;
				now++;
				break;
			}
					
		}
	}
	return now;
}