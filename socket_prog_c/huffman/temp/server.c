#include "huffman.h"



map<int,int> count;													//To count the times of the characters.
map<int,array> encode_table;										//To record the encoding table. And can use to decoding.
map<array,int> decode_table;
unsigned char *un_code;
char *bit_code;
unsigned char *com_code;
int exceed;		
priority_queue<treenode*, vector<treenode*>, mycompare> p_queue;

int main()
{
	int ser_fd,cli_fd;
	struct sockaddr_in server,client;
	int clientlen = sizeof(client);

	/*Use socket to get fd of server.*/
	if((ser_fd = socket(AF_INET,SOCK_STREAM,0))<0)
		ERROR("socket");

	/**/
	memset(&server,'\0',sizeof(server));

	server.sin_family = AF_INET;
	/*Setup port*/
	server.sin_port = htons(PORTNUM);
	/* bind: a client may connect to any of my addresses */
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	
	/*
	int setsockopt(int sockfd, int level, int optname,
                  const void *optval, socklen_t optlen);
    get and set options on sockets.
	
	To set options at the socket level, specify the level argument as SOL_SOCKET.
	SO_REUSEADDR:Specifies that the rules used in validating 
	addresses supplied to bind() should allow reuse of local 
	addresses, if this is supported by the protocol.
	Without this line, the address may not be used after terminate the 
	program and restart it.
	*/
	int opt = 1;
	setsockopt(ser_fd,SOL_SOCKET,SO_REUSEADDR,&opt,(socklen_t)clientlen);
	/**/
	if(bind(ser_fd,(struct sockaddr *)&server,sizeof(server)) <0)
		ERROR("bind");

	


		if(listen(ser_fd, 128) <0)
			ERROR("listen");
	while(1)
	{
		/*gcc can just use &clientlen, but g++ has to change to (socklen_t *)&clientlen*/
		if((cli_fd = accept(ser_fd,(struct sockaddr *)&client,(socklen_t *)&clientlen)) == -1)
			ERROR("accept");

		/*Display client information*/
		/*inet_ntoa(in_addr ) : change in_addr(unsigned long) into ip address*/
		int fork_fd = fork();
	
		if(fork_fd==-1)
			ERROR("fork");
		else if(fork_fd==0)
		{
			close(ser_fd);

	
			printf("Connect succeed!\n");
			printf("Information : \n");
			printf("Client's ip : %s\n",inet_ntoa(client.sin_addr));
			printf("Client's port : %d\n",client.sin_port);
			printf("Socket type : SOCK_STREAM(TCP)\n");

			/*Use fixed-length(512) to read.*/
			/*Read the compression file imformation.*/

			char filename[128];
			char message1[512]="\0";	

			while(read(cli_fd,message1,sizeof(message1)))
			{


				exceed = 0;	
				

				long before_size;
				int after_size;
				double rate;
				sscanf(message1,"%s %ld %d %lf",filename,&before_size,&after_size,&rate);
				//printf("%s %ld %d %lf",filename,before_size,after_size,rate);
				
				cout<<"The client sends a file \""<<filename<<"\" with size of "<<before_size<<" bytes"<<endl;
				

				if(strcmp(filename,"0")==0 && before_size==0)
					break;

				char str_encode[128];
				read(cli_fd,str_encode,sizeof(str_encode));


				FILE *encode_file;
				/*Write encoding table to file*/
				char encode_filename[128];
				strcpy(encode_filename,filename);

				char *test = strtok(encode_filename,".");
				strcpy(encode_filename,test);
				strcat(encode_filename,"-code.txt");

				cout<<"The huffman coding data are stored in \""<<encode_filename<<"\"."<<endl;

				encode_file = fopen(encode_filename,"w");
				fprintf(encode_file,"Alphabet(ASCII code)\t"
									"Frequency(%%)\t\t\t"
									"Codeword\n");


				/*Get the encode_table*/
				/*ASCII code*/
				int char_num;
				/*Codeword length*/
				int encode_size;
				/*The times of character*/
				int encode_times;
				/*encode bit*/
				char encode_num[8];

				while(strcmp(str_encode,"\n")!=0)
				{
					//printf("%s",str_encode);

					sscanf(str_encode,"%d %d %d %s",&char_num,&encode_size,&encode_times,encode_num);
					//printf("%d %d %s\n",char_num,encode_size,encode_num);

					fprintf(encode_file,"%c(%d)\t\t\t\t\t%d(%0.2lf%%)\t\t\t\t%s\n",char_num,char_num,encode_times,(encode_times/(before_size*1.0))*100,encode_num);
					




					read(cli_fd,str_encode,sizeof(str_encode));

					strcpy(encode_table[char_num].a,encode_num);
					encode_table[char_num].size = encode_size;
					encode_table[char_num].times = encode_times;

				}

				/*Remember to close*/
				fclose(encode_file);

				

				char first_num;
				com_code = (unsigned char*)malloc(after_size*sizeof(unsigned char));
				
				/*Read excessive number*/
				read(cli_fd,&first_num,1);
				/*Read whole file*/
				read(cli_fd,com_code,after_size);

				exceed = first_num - 48;

				/*=======Start to uncompress data=========*/

				bit_code = (char *)malloc(after_size*8);

				/*Convert compresstion data to binary code*/
				int bit_size = com_to_bit(after_size);

				un_code = (unsigned char *)malloc((before_size+1)*sizeof(unsigned char));

				/*Umcompress data.*/
				int now = umcompress_data(bit_size);						
				
				/*Write the file in byte mod.*/
				FILE *file4;
				file4 = fopen(filename,"wb");

				fwrite(un_code,sizeof(un_code[0]),now,file4);
					
				fclose(file4);
			}
			cout<<"The client \""<<inet_ntoa(client.sin_addr)<<"\"";
			cout<<" with port "<<client.sin_port<<" has terminated the connection"<<endl;
			close(cli_fd);
			exit(0);
		}
		else
			close(cli_fd);	
	}
	//printf("\nFinished!!\n");							

	return 0;
}