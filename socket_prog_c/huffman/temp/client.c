#include "huffman.h"

map<int,int> count;													//To count the times of the characters.
map<int,array> encode_table;										//To record the encoding table. And can use to decoding.
map<array,int> decode_table;
unsigned char *un_code;
char *bit_code;
unsigned char *com_code;
int exceed;	
priority_queue<treenode*, vector<treenode*>, mycompare> p_queue;


int main(int argc, char **argv)
{	
	int ser_fd;
	struct sockaddr_in server;
	struct hostent *host;
	char *end;

	if(argc!=3)
		ERROR("Usage : <ipaddress><port>\n");

	server.sin_family = AF_INET;
	server.sin_family = AF_INET;

	/*Translate the port number*/
	int PORT = strtol(argv[2],&end,10);
	server.sin_port = htons(PORT);

	/*Use DNS to transform ip address*/
	if((host = gethostbyname(argv[1])) == NULL)
		ERROR("gethostbyname");
	/*Setup ip address using host*/
	memcpy((char *)&server.sin_addr, host->h_addr,host->h_length);

	/*Create socket*/
	if((ser_fd = socket(AF_INET, SOCK_STREAM, 0)) <0)
		ERROR("socket");

	/*Connect to the server*/
	if(connect(ser_fd, (struct sockaddr *)&server,sizeof(server)))
		ERROR("connect");


	int out = 1;
	while(out)
	{
		printf("$:");
		char command[128];
		char ins[128];
		char filename[128];

		fgets(command,128,stdin);
		
		memset(filename,0,sizeof(filename));

		sscanf(command,"%s %s",ins,filename);



		if(strcmp(ins,"logout")==0)
		{
			cout<<"Goodbye!"<<endl;
			close(ser_fd);
			out = 0;
			continue;
		}
		else if(strcmp(ins,"send")!=0)
		{
			printf("Usage: logout | send <filename>\n");
			continue;
		}

		/*Check the existence of the filename.*/
        if(access(filename,F_OK)==-1)
        {
             printf("Invalid filename\n");
             continue;
        }



        /*=========Start to huffman's compression=======*/
        exceed = 0;
		FILE *file1;

		long filelen;
		file1 = fopen(filename,"rb");

		/*Jump to the end of the file*/
		fseek(file1,0,SEEK_END);
		/*Calculate the number of bytes of the file.*/								
		filelen = ftell(file1);										
		/*Jump back to the begining of the file.*/
		rewind(file1);												
		
		un_code = (unsigned char *)malloc((filelen+1)*sizeof(unsigned char));
		/*fread will read all of the characters in the file.*/
		fread(un_code,filelen,1,file1);

		/*For fixed-length huffman*/
		/*Start from -1, deal with the situation when there are 256 characters.*/
		int number_of_types = -1;

		/*Initiailize the content of these container.s*/
		count.clear();
		encode_table.clear();
		priority_queue<treenode*, vector<treenode*>, mycompare> empty;
   		swap(p_queue,empty);

		for(int i=0;i<filelen;i++)
		{
			/*Count the number of types*/
			/*For fixed-length*/
			map<int,int>::iterator it;
			it = count.find(un_code[i]);
			if(it==count.end())
				number_of_types++;

			/*Count the appearing times.*/
			/*For variabled-length*/
			count[un_code[i]]++;
		}										

		fclose(file1);
		
		cout<<"What type of huffman compression do you want?(type 1 or 2)"<<endl;
		cout<<"1 fixed-length"<<endl;
		cout<<"2 variable-length"<<endl<<"$:";
		int type,fixed_length;
		cin>>type;
		getchar();
		if(type==1)
		{
			fixed_length = huffman_encoding_fixed_length(number_of_types);
		}
		else if(type==2)
		{
			/*Create priority queue.*/
			create_p_queue();											
			/*Build the huffman tree.*/
			build_huffman_tree();										
				
			/*Build encoding table.*/
			char str[9];
			huffman_encoding(p_queue.top(),str,0);	
		}
		else
		{
			cout<<"Invalid input"<<endl;
			continue;
		}
		


		bit_code = (char*)malloc(filelen*8);
		int bit_size = bit_with_encoding(filelen);
		
		
		com_code = (unsigned char*)malloc((bit_size+1)*sizeof(unsigned char));
		int now = compress_data(bit_size);
		

		cout<<"The original file length : "<<filelen<<" bytes"<<endl;
		cout<<"The compressed file length : "<<now<<" bytes"<<endl;
		cout<<"The compression ratio : "<<(filelen-now)/(filelen*1.0)*100<<"%"<<endl;
		if(type==1)
		{
			printf("Using fixed-length codeword(%d-bit)\n",fixed_length);
		}

		/*Use fixed-length to write in order to make server can read easilier.*/
		/*Socket*/
		char message[512];
		memset(message,'\0',sizeof(message));
		sprintf(message,"%s %ld %d %lf\n",filename,filelen,now,(filelen-now)/(filelen*1.0)*100);
		write(ser_fd,message,sizeof(message));
		


		/*Write encoding table.*/
		map<int,array>::iterator it2;
		for(it2=encode_table.begin();it2!=encode_table.end();it2++)
		{
			/*Socket*/
			char str_encode[128];
			memset(str_encode,'\0',sizeof(str_encode));
			sprintf(str_encode,"%d %d %d %s\n",it2->first,it2->second.size,it2->second.times,it2->second.a);
			write(ser_fd,str_encode,sizeof(str_encode));

		}

		/*Socket*/
		/*Write a newline to separate the data information and data*/
		char str_newline[128];
		memset(str_newline,'\0',sizeof(str_newline));
		str_newline[0] = '\n';
		write(ser_fd,str_newline,sizeof(str_newline));

		/*Write the data,including exceed and bitcode*/
		dprintf(ser_fd,"%d",exceed);
		write(ser_fd,com_code,now);

		
	}
	


	return 0;
}