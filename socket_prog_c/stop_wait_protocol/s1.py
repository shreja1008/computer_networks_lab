#stop and wait protocol

import socket
import time 
import random
#------------------------------------------------------------------------
#create socket object and bind it.
print("                        STOP AND WAIT PROTOCOL  ")
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostbyname(socket.gethostname())
port=input("Enter the port number(preferably above 1000):")
addr=(host,int(port))
server.bind(addr)

server.listen(3)
print("[SERVER] listening at ip ...",host,"port"+str(port))

cli,conn=server.accept()
cli.settimeout(0.6)
print("[CLIENT] connected :)")

#enter the message to be sent 
buf=input("Enter the message:")
#send the messgae frame by frame and wait for ack after every frame 
i=0
while i < len(buf):
    frame=buf[i]
    cli.send(bytes(frame,"utf8"))
    print("msg"+str(i),"sent "+str(buf[i]))
    time.sleep(.3)
    ack=int(cli.recv(1024).decode())
    if(ack):
        print("Sent xD")
    else:
        print("Not sent please try again :(")
        
    i=i+1
server.close()