import socket
import time 

#------------------------------------------------------------------------
#create socket object and bind it.
print("                        STOP AND WAIT PROTOCOL  ")

cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostbyname(socket.gethostname())
port=input("Enter the port number(preferably above 1000):")
addr=(host,int(port))
cli.connect(addr)

frame = cli.recv(1024).decode()

ack=1
perv_frame=1
while(frame):
    print(frame,end='',flush=True)
    cli.send(bytes(str(ack),"utf8"))

    frame =cli.recv(1024).decode()

cli.close()
