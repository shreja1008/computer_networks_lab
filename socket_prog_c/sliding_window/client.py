import socket
import time 

#------------------------------------------------------------------------
#create socket object and bind it.
print("                        SLIDING WINDOW PROTOCOL  ")

cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostbyname(socket.gethostname())
port=input("Enter the port number(preferably above 1000):")
addr=(host,int(port))
cli.connect(addr)

#input the window size and send it to the reciever
window_size=input("Enter the window size:")

cli.send(bytes(window_size,"utf8"))

#input the message to be sent 
msg=input("Enter the message to be sent:")

#send the message according to the window size specified

i=0
flag='1'

for i in range(0,len(msg)):
    cli.send(bytes(flag,"utf8"))
    for j in range(0,int(window_size)):
        cli.send(bytes(msg[i+j],"utf8"))
        print("Msg sent: "+str(msg[i+j]))
    ack_check=-1
    for j in range(0,int(window_size)):
        ack=cli.recv(1024).decode()
        print("ack:"+str(ack))
        if(ack==0):
            ack_check=1
            j=int(window_size)
        
    if(ack_check==-1):
        i+=(int(window_size)-1)
    else:
        i+=(ack_check-1)
flag='0'
cli.send(bytes(flag,"utf8"))

# cli.send(bytes(n,"utf8"))
print("Message sent sucessfully :) ")
cli.close()