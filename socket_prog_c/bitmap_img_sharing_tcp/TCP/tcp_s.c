#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<string.h>
#include<time.h>
#include<unistd.h>
int main()
{
    long double start,end,total;
    int s_sock,c_sock,f=1;
    char buff[200],f_name[100];
    struct sockaddr_in server, other ;
    socklen_t addr_size;
   

    s_sock=socket(AF_INET,SOCK_STREAM,0);
    server.sin_addr.s_addr=INADDR_ANY;
    server.sin_port=htons(9009);
    server.sin_family=AF_INET;

    if(bind(s_sock,(struct sockaddr*)&server,sizeof(server))==-1){
        printf("Unable to connect!!\n");
        exit(-1);
    }
    listen(s_sock,10);
    addr_size=sizeof(other);
    c_sock=accept(s_sock,(struct sockaddr*)&other,&addr_size);
    recv(c_sock,&f,sizeof(f),0);
    if(f!=1){
        printf("operation failed\n");
        exit(-1);
    }
    else{
        printf("file being transferred......\n");
    }
    start = clock();
    recv(c_sock,f_name,sizeof(f_name),0);
    FILE *destptr;
    destptr=fopen(f_name,"wb");
    int c;
    read(c_sock,&c,sizeof(c));
    while (c != EOF){ 
        fputc(c, destptr); 
        read(c_sock,&c,sizeof(c));
    } 
    printf("\nContents copied to %s", f_name);
    printf("\n"); 
    end = clock();
    total = (double)(end - start) / CLOCKS_PER_SEC;
    printf("latency(cpu time/no of bytes): %Lf\n", total/80640);
    fclose(destptr); 
    close(s_sock);
    return 0;


}