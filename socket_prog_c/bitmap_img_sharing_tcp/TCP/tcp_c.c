#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<string.h>
#include<time.h>
#include<unistd.h>

int main()
{
   
    int c_sock,flag=1;
    char buffer[200],send_file[100],dest_file[100];
    struct sockaddr_in client;


    c_sock=socket(AF_INET,SOCK_STREAM,0);
    client.sin_family=AF_INET;
    client.sin_port=htons(9009);
    client.sin_addr.s_addr=INADDR_ANY;

    if(connect(c_sock,(struct sockaddr*)&client,sizeof(client))==-1)
    {
        printf("Unable to connect!!\n");
        exit(-1);
    }
    else
        printf("connection established\n");
    
    printf("enter file name to be sent: ");
    scanf("%s",send_file);

    FILE *srcptr;
    srcptr=fopen(send_file,"rb");
    if(srcptr==NULL)
    {
        printf("Error copying\n");
        flag=0;
        send(c_sock,&flag,sizeof(flag),0);
        exit(1);
    }

    printf("enter new file name to save the sent file as: ");
    scanf("%s",dest_file);

    send(c_sock,&flag,sizeof(flag),0);
    send(c_sock,dest_file,sizeof(dest_file),0);
    int c;
	while((c=fgetc(srcptr))!=EOF)
    {
        write(c_sock,&c,sizeof(c));
	}
	c=EOF;
    write(c_sock,&c,sizeof(c));
    printf("file sent successfully\n");
    fclose(srcptr);
    close(c_sock);
    return 0;
}