//socket programming(client.c)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include<ctype.h>

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[1024];
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(9009);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
  
  
    int num1 ;
    int num2 ;
    float ans;
    int choice;
    /* Remove if doesnt work*/ 
    
    printf("\n\nWelcome To TCP/IP Calculator\n\n");
    R:bzero(buffer,256);						
    n = read(sockfd,buffer,255);			//Read Server String
    if (n < 0) 
         error("ERROR reading from socket");
    printf("Server - %s",buffer);
    scanf("%d" , &choice);				//Enter choice
    write(sockfd, &choice, sizeof(int));     		//Send choice to Server

	if (choice == 5)
	goto Q;

	//if(choice != 5)
    	//goto S;

   // read(sockfd , &ans , sizeof(int));		       //Read Answer from Server
    //printf("Server - The answer is : %d\n" , ans);  //Get Answer from server    

	
    //S:
    bzero(buffer,256);						
    n = read(sockfd,buffer,255);			//Read Server String
    if (n < 0) 
         error("ERROR reading from socket");
    printf("\nServer - %s",buffer);
    scanf("%d" , &num1);				//Enter No 1
    write(sockfd, &num1, sizeof(int));     		//Send No 1 to Server
	
	
	
    bzero(buffer,256);						
    n = read(sockfd,buffer,255);			//Read Server String
    if (n < 0) 
         error("ERROR reading from socket");
    printf("\nServer - %s",buffer);
    scanf("%d" , &num2);				//Enter No 2
    write(sockfd, &num2, sizeof(int));     		//Send No 2 to Server
	
	
   /* 	
    bzero(buffer,256);						
    n = read(sockfd,buffer,255);			//Read Server String
    if (n < 0) 
         error("ERROR reading from socket");
    printf("Server - %s",buffer);
    scanf("%d" , &choice);				//Enter choice
    write(sockfd, &choice, sizeof(int));     		//Send choice to Server
	if (choice == 5)
	goto Q;
	 if(choice != 5)
    	goto S;
*/

    read(sockfd , &ans , sizeof(float));	       //Read Answer from Server
    printf("\nServer - \nThe answer is : %.2f\n\n\n" , ans);	//Get Answer from server
    printf("\nRerunning calculator...\n\n");
    goto R;

   
Q:  printf("\n\nYou chose to exit, Thank You so much.\n\n");
     	
       
   
    
    close(sockfd);
    return 0;
}

