#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include<ctype.h>
int main(){
    int s_sock,c_sock;
    char buf[20];
    char msg[100]="Server welcomes you to the calculator\n";
    char menu[100] = "Choose one of the following options\n" "1: add\n2: subtract\n3: multiply\n4: divide\n5: exit\n";
    float op1,op2;
    int oper;
    struct sockaddr_in server,other;
    socklen_t add;
    memset(&server,0,sizeof(server));
    memset(&other,0,sizeof(other));

    s_sock=socket(AF_INET,SOCK_STREAM,0);
    server.sin_family=AF_INET;
    server.sin_port=htons(9009);
    server.sin_addr.s_addr=INADDR_ANY;

    bind(s_sock,(struct sockaddr*)&server,sizeof(server));
    listen(s_sock,10);
    printf("waiting for client...\n");
    add=sizeof(other);
    c_sock=accept(s_sock,(struct sockaddr*)&other,&add);
    if(c_sock==-1)
    {
        printf("could not connect to client");
        return 0;
    }
    else
    {
        printf("connected to client\n\n");
    }
    // send(c_sock,msg,sizeof(msg),0);
    // printf("Hi");
    while(1){
        send(c_sock,menu,sizeof(menu),0);
        //recv(c_sock,buf,sizeof(buf),0);
        recv(c_sock,&oper,sizeof(oper),0);
        printf("operator recieved\n");
        recv(c_sock,&op1,sizeof(op1),0);
        printf("operand 1 recieved\n");
        recv(c_sock,&op2,sizeof(op2),0);
        printf("operand 2 recieved\n");
        
        switch (oper) {
        case 1:
            // printf("%.1lf + %.1lf = %.1lf", op1, op2, op1 + op2);
            snprintf(msg, sizeof msg, "%f", op1 + op2);
            break;
        case 2:
            // printf("%.1lf - %.1lf = %.1lf", op1, op2, op1 - op2);
            snprintf(msg, sizeof msg, "%f", op1 - op2);
            break;
        case 3:
            // printf("%.1lf * %.1lf = %.1lf", op1, op2, op1 * op2);
            snprintf(msg, sizeof msg, "%f", op1 * op2);
            break;
        case 4:
            // printf("%.1lf / %.1lf = %.1lf", op1, op2, op1 / op2);
            snprintf(msg, sizeof msg, "%f", op1 / op2);
            break;
        case 5:
            printf("exiting...\n");
            close(s_sock);
            exit(0);
            break;
        default:
            memcpy(msg,"Invalid input",sizeof msg);
            break;
        }

        send(c_sock,msg,sizeof(msg),0);
        printf("result sent\n\n");
    }
    return 0;
}