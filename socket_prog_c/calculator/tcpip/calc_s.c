#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
void error(const char *msg)
{
	perror(msg);
	exit(1);
}
//argc=no of connections it accepts
//arg
int main(int argc, char *argv[])
{						
    int sockfd,newsockfd,n;
	char buffer[255];
	
	struct sockaddr_in serv_addr,cli_addr; //sockaddr gives internet addr
	socklen_t clilen;   //socklen_t is datatype of 4 bytes in socket.h
	sockfd=socket(AF_INET,SOCK_STREAM,6);  //creates socket
	if(sockfd < 0)
	{
		error("\nError genarting the socket. ");	
	}
	bzero((char *) &serv_addr, sizeof(serv_addr)); 
	//clears all the data whatever it refers to
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=INADDR_ANY;
	serv_addr.sin_port=htons(9009);
	if(bind(sockfd,(struct sockaddr*)&serv_addr,sizeof(serv_addr))<0)
		error("\nBinding failed.");
	
	listen(sockfd,5);  //5 denotes number of users
	clilen=sizeof(cli_addr);
	
	newsockfd=accept(sockfd,(struct sockaddr*)&cli_addr, &clilen);
	
	if(newsockfd<0)
		error("\nError on accepting.");
	
	int num1,num2,choice;
	float ans;
	R:n=write(newsockfd,"\nMenu :\n1.Addition\n2.Subtraction\n3.Multiplication\n4.Division\n5.Exit\n\nEnter Choice : ",strlen("\nMenu :\n1.Addition\n2.Subtraction\n3.Multiplication\n4.Division\n5.Exit\n\nEnter Choice : "));
	if(n<0)
		error("\nError writing to socket");
	read(newsockfd, &choice, sizeof(int));
	printf("\nClient - Choice is : %d\n",choice);
	//if(choice!=5)
	//	goto S;
	if(choice==5)
		goto Q;
	//S:
	n=write(newsockfd,"\nEnter Number 1 : ",strlen("\nEnter Number 1 : "));
	if(n<0)
		error("\nError writing to socket");
	read(newsockfd, &num1, sizeof(int));
	printf("\nClient - Number 1 is : %d",num1);
	
	
	n=write(newsockfd,"\nEnter Number 2 : ",strlen("\nEnter Number 2 : "));
	if(n<0)
		error("\nError writing to socket");
	read(newsockfd, &num2, sizeof(int));
	printf("\nClient - Number 2 is : %d\n",num2);
	/*
	n=write(newsockfd,"\nEnter Choice :\n1.Addition\n2.Subtraction\n3.Multiplication\n4.Division\n5.Exit ",strlen("\nEnter Choice :\n1.Addition\n2.Subtraction\n3.Multiplication\n4.Division\n5.Exit "));
	if(n<0)
		error("\nError writing to socket");
	read(newsockfd, &choice, sizeof(int));
	printf("\nClient - Choice is : %d",choice);
	*/
	
	switch(choice)
	{
		case 1:
			ans=num1+num2;
			break;
		case 2:
			ans=num1-num2;
			break;
		case 3:
			ans=num1*num2;
			break;
		case 4:
			ans=num1/num2;
			break;
		case 5:
			goto Q;
			break;
	}
	
	write(newsockfd,&ans,sizeof(float));
	printf("\n\nRerunning calculator...\n\n");	
	goto R;
	
	
	Q:close(newsockfd);  //closes newsockfd
	close(sockfd);  //closes sockfd
	return 0;
}
 