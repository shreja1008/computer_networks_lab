#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char mensajeRec[3];
char mensajeEnv[2];

struct sockaddr_in dirMia;
struct sockaddr_in sock;

int rec, result;
int port;
int sk;
int i, componente;
int op_1, op_2, result;

int main (int argc, char *argv[]) {
    if (argc < 1) {
        printf("Error al invocar el programa. Formato llamada server [port]. Ej: server 7777.\n");
        exit(1);
    } 
    //else {
        //port = atoi(argv[1]);
    //}
    sk = socket(AF_INET, SOCK_DGRAM, 0);
    dirMia.sin_family = AF_INET;
    dirMia.sin_addr.s_addr = INADDR_ANY;
    dirMia.sin_port = htons(port);
    result = bind(sk, (struct sockaddr*)&dirMia, sizeof(dirMia));
    // if (result < 0) {
    //     printf("Error al crear el socket en el puerto %d\n", port);
    //     exit(1);
    // }else{
    //     printf("Escuchando en puerto %d\n", port);
    // }

    while (result >= 0) {
        bzero(&sock,sizeof(sock));
        int add = sizeof(sock);
        recvfrom(sk, mensajeRec, sizeof(mensajeRec), 0, (struct sockaddr*)&sock, (unsigned*)&add);
        op_1 = mensajeRec[1] - '0';
        op_2 = mensajeRec[2] - '0';
        switch(mensajeRec[0]) {
            case '+':
                result = op_1+op_2;
                break;
            case '-':
                result = op_1-op_2;
                break;
            case '/':
                result = op_1/op_2;
                break;
            case '*':
                result = op_1*op_2;
                break;
        }
        mensajeEnv[0] = result/10 + '0';
        mensajeEnv[1] = result%10 + '0';
        result = sendto(sk,mensajeEnv,strlen(mensajeEnv),0,(struct sockaddr*)&sock,sizeof(sock));
    }
    exit(0);
}