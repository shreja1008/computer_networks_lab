#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct sockaddr_in sock;

char op_1[3];
char op_2[2];

int port;

int c_sock, client;

int main (int argc, char *argv[]) {
    if (argc < 4) {
        printf("Error al invocar el programa. Formato llamada client [port] [operacion] [operando1] [operando2]. Ej: cliente 7777 + 5 2.\n");
        printf("Nota: El parámetro multiplicación '*' se deberá enviar con el caracter backslash delante '\\'\n");
        exit(1);
    } else {
       // port = atoi(argv[4]);
        printf("Se va a enviar en puerto %d\n", port);
        op_1[0] = *argv[1];
        op_1[1] = *argv[2];
        op_1[2] = *argv[3];
    }

    c_sock=socket (AF_INET, SOCK_DGRAM,0);
    sock.sin_family= AF_INET;
    sock.sin_addr.s_addr= INADDR_ANY;
    sock.sin_port= htons(9009);

    sendto(c_sock,op_1,strlen(op_1),0,(struct sockaddr*)&sock,sizeof(sock));

    int add= sizeof(sock);

    client= recvfrom(c_sock
,op_2,sizeof(op_2), 0, (struct sockaddr*)&sock, (unsigned*) &add);
    op_2[client] = '\0';
    printf("result: %s\n", op_2);

    exit(0);
}