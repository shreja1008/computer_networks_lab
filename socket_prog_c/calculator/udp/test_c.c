#include<stdio.h>
#include<string.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
main()
{
    struct sockaddr_in client,server;
    int s_sock,n;
    char buff[100],msg[100];
    s_sock=socket(AF_INET,SOCK_DGRAM,0);
    server.sin_family=AF_INET;
    server.sin_port=htons(9009);
    server.sin_addr.s_addr=INADDR_ANY;
    printf("\nClient ready....\n");

    n=sizeof(server);
    while(1)
    {
        printf("\nClient:");
        gets(msg);
        sendto(s_sock,msg,sizeof(msg),0,(struct sockaddr *) &server,n);
        if(strcmp(msg,"quit")==0)
            break;
        recvfrom(s_sock,buff,sizeof(buff),0,NULL,NULL);
        printf("\nServer:%s",buff);
    }
close(s_sock);
}
