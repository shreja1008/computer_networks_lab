#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<string.h>
#include<arpa/inet.h>

int cli_sock;
struct sockaddr_in server_socket_addr, client_sock_addr;
socklen_t addr_len;


int main (int argc, char* argv[]){


	cli_sock = socket(AF_INET, SOCK_STREAM, 0);
	if(cli_sock < 0){
		printf("Could not create socket\n");
		exit(0);
	}

	memset(&server_socket_addr, 0, sizeof(server_socket_addr)); 
	memset(&client_sock_addr, 0, sizeof(client_sock_addr)); 

	char ip[20],sourcefile[20];
	int port;

	printf("Enter IP of server : ");
	scanf("%s",ip);
	printf("Enter port number : ");
	scanf("%d",&port);
	printf("Enter filename of file to be downloaded : ");
	scanf("%s",sourcefile);

	server_socket_addr.sin_family = AF_INET;
	server_socket_addr.sin_addr.s_addr = inet_addr(ip);
	server_socket_addr.sin_port = htons(port);

	socklen_t addr_len;
	if(connect(cli_sock, (struct sockaddr*)&server_socket_addr, sizeof(server_socket_addr))<0){
		printf("Connection failed\n");
		exit(-1);
	}

	printf("[+] Connected with server\n");

	char src_file[100];
	strcpy(src_file, sourcefile);
	send(cli_sock, src_file, sizeof(src_file), 0);

	FILE *dest = fopen(src_file, "wb");

	printf("\n[!] Receiving file\n");

	int c;
	recv(cli_sock, &c, sizeof(c), 0);
	while(c != EOF){
		fputc(c, dest);
		// printf("%d\n", c);
		recv(cli_sock, &c, sizeof(c), 0);
	}

	printf("\n[+] New file created with name at server_files: %s\n", src_file);
	printf("[!] Rename the file suitably!\n");

	fclose(dest);

	return 0;
}
