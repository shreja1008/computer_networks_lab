#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<string.h>

int ser_sock, cli_sock;
struct sockaddr_in server_socket_addr, client_sock_addr;
socklen_t addr_len;

int main (){

	ser_sock = socket(AF_INET, SOCK_STREAM, 0);
	if(ser_sock < 0){
		printf("Could not create socket\n");
		exit(0);
	}

	memset(&server_socket_addr, 0, sizeof(server_socket_addr)); 
	memset(&client_sock_addr, 0, sizeof(client_sock_addr)); 

	server_socket_addr.sin_family = AF_INET;
	server_socket_addr.sin_addr.s_addr = INADDR_ANY;
	server_socket_addr.sin_port = htons(23456);
	
	if(bind(ser_sock, (struct sockaddr*)&server_socket_addr, sizeof(server_socket_addr)) < 0){
		printf("Bind failed\n");
		exit(0);
	}

	printf("[+] Server Running\n");

	socklen_t addr_len;
	listen(ser_sock, 2);
	addr_len = sizeof(client_sock_addr);
	
	cli_sock = accept(ser_sock, (struct sockaddr*)&client_sock_addr, &addr_len);
	printf("[+] Client Connected\n");

	char file_name[100], file_copy[100] = "server_files/";
	recv(cli_sock, file_name, sizeof(file_name), 0);
	strcat(file_copy, file_name);

	FILE *src;
	src = fopen(file_copy, "rb");

	printf("\n[!] Sending file\n");

	int c;
	// clock_t start=clock(), end;
	while((c = fgetc(src)) != EOF){
		// printf("%d\n", c);
		send(cli_sock, &c, sizeof(c), 0);
	}
	c = EOF;
	send(cli_sock, &c, sizeof(c), 0);
	// end = clock();	

	// printf("[!] Time taken: %f\n", (double)(end-start)/CLOCKS_PER_SEC);
	// printf("\n[$] File sent. Terminating connection\n");

	fclose(src);

	return 0;
}

