#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<string.h>
#include<netinet/in.h>
int main(){
int s_sock;
char buf[20];
s_sock=socket(AF_INET,SOCK_DGRAM,0);
struct sockaddr_in server,other;
server.sin_family=AF_INET;
server.sin_port=htons(9009);
server.sin_addr.s_addr=INADDR_ANY;
bind(s_sock,(struct sockaddr*)&server,sizeof(server));
socklen_t add;
add=sizeof(other);
recvfrom(s_sock,buf,sizeof(buf),0,(struct sockaddr*)&other,&add);
printf("msg from the client:%s\n",buf);
close(s_sock);
}