#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<string.h>
#include<netinet/in.h>
int main(){
int c_sock; //client socket creation 
char buf[20]="hey i am shreja"; //msg to be displayed in the server from the client
c_sock=socket(AF_INET,SOCK_DGRAM,0);//init a socket api  udp  conn with proper arg
struct sockaddr_in client;//struct to save socket info in client
client.sin_family=AF_INET;     //saving the 
client.sin_port=htons(9009);   // socket info in client(use any port other than reserved ports like 80 http )
client.sin_addr.s_addr=INADDR_ANY;//allowing access to any address port
sendto(c_sock,buf,sizeof(buf),0,(struct sockaddr*)&client,sizeof(client));
printf("client to server msg:%s\n",buf);
close(c_sock);
}  
