#include<stdio.h>
#include<string.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
main()
{
    struct sockaddr_in client,server;
    int s_sock,n;
    char buff[100],msg[100];
    s_sock=socket(AF_INET,SOCK_DGRAM,0);
    server.sin_family=AF_INET;
    server.sin_port=htons(9009);
    server.sin_addr.s_addr=INADDR_ANY;
    bind(s_sock,(struct sockaddr *)&server,sizeof(server));
    printf("\nServer ready,waiting for client....\n");
    n=sizeof(client);
    while(1)
    {
        recvfrom(s_sock,buff,sizeof(buff),0,(struct sockaddr *) &client,&n);
        if(!(strcmp(buff,"quit")))
            break;
        printf("\nClient:%s",buff);
        printf("\nServer:");
        gets(msg);
        sendto(s_sock,msg,sizeof(msg),0,(struct sockaddr *) &client,n);
              
    }
 close(s_sock);
}